$(function() {
    $('[data-toggle="tooltip"]').tooltip({
        placement: 'top'
    });
});

function delItem(id){
    if(confirm('Удалить это расписание?')){
        $.ajax({
            type: "POST",
            url: "/ajax/delete_scheldure",
            data: JSON.stringify({'id':id}),
            success: function(html){
                $('#table').html(html);
            }
        });
    }
}

function filterDist(val){
    $.ajax({
        type: "POST",
        url: "/ajax/filter_dist",
        data: JSON.stringify({'filter':val}),
        success: function(html){
            $('#table').html(html);
        }
    });
}