<?php defined('SYSPATH') or die('No direct script access.');

return array(
    'time' => 'Время Отправления',
    'dist' => 'Пункт Назначения',
    'days' => 'График Движения',
    'days.all' => 'Ежедневно',
    'days.work' => 'Только по рабочим',
    'days.weekend' => 'Только по выходным',
    'save' => 'Сохранить',
    'cancel' => 'Отмена',
    'edit.success' => 'Рассписание успешно обновлено',
    'create.success' => 'Новое Рассписание успешно создано',
);