<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @var $query string
 * @var $table
 */
?>

<form action="/search" method="post">
    <div class="form-group">
        <label for="search" class="sr-only"></label>
        <div class="input-group">
            <input type="text" name="search" id="search" class="form-control" placeholder="Введите Пункт Назначения" value="<?php echo $query; ?>">
            <span class="input-group-btn">
                <button type="submit" class="btn btn-primary">Искать</button>
            </span>
        </div>
    </div>
    <div class="form-group">

    </div>
</form>

<div id="table" class="table-responsive">
    <?php echo $table; ?>
</div>
