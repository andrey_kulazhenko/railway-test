<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @var $action string Current action
 */
?>

<?php if($action == 'index') { ?>
    <a href="/create">Создать новое <i class="fa fa-plus-square"></i></a>
<?php } else { ?>
    <a href="/">Вернуться на главную <i class="fa fa-home"></i></a>
<?php } ?>