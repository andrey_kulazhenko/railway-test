<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @var $content
 * @var $sidebar
 * @var $header
 */
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>RailWay</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/style.css">
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/assets/js/jquery-1.11.3.min.js"></script>
    <script src="/assets/js/bootstrap.min.js"></script>
    <script src="/assets/js/script.js"></script>
</head>
<body>
<div class="container" id="container">
    <h1 class="page-header"><?php echo $header; ?></h1>
    <div>
        <?php echo Helper_Message::showAlerts(); ?>
    </div>
    <div class="row">
        <div class="col-sm-3 col-md-2 text-center"><?php echo $sidebar; ?></div>
        <div id="content" class="col-sm-9 col-md-10"><?php echo $content; ?></div>
    </div>
</div>
</body>
</html>
