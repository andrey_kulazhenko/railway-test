<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @var $table
 */
?>

<div class="text-right">
    <a data-toggle="collapse" href="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">Фильтр <i class="fa fa-filter"></i></a>
    <a data-toggle="collapse" href="#collapseSearch" aria-expanded="false" aria-controls="collapseSearch">Поиск <i class="fa fa-search"></i></a>
    <div class="collapse" id="collapseFilter">
        <div class="well text-left">
            <label for="filter" class="sr-only"></label>
            <input type="text" name="filter" id="filter" class="form-control" onkeyup="filterDist($(this).val());" placeholder="Начните вводить название Пункта Назначения...">
        </div>
    </div>
    <div class="collapse" id="collapseSearch">
        <div class="well text-left">
            <form action="/search" method="post">
                <div class="form-group">
                    <label for="search" class="sr-only"></label>
                    <div class="input-group">
                        <input type="text" name="search" id="search" class="form-control" placeholder="Введите Пункт Назначения">
                          <span class="input-group-btn">
                            <button type="submit" class="btn btn-primary">Искать</button>
                          </span>
                    </div>
                    <p class="help-block">
                        <small>Поиск ближайшего Электропоезда</small>
                    </p>
                </div>
                <div class="form-group">

                </div>
            </form>

        </div>
    </div>
</div>


<div id="table" class="table-responsive">
    <?php echo $table; ?>
</div>
