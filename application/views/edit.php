<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @var $data object Model_Scheldure
 */
?>

<div class="row-fluid">
    <div class="col-sm-6 col-sm-offset-3">
        <form action="" method="post">
            <div class="form-group">
                <label for="time" class="control-label"><?php echo __('time') ?></label>
                <input type="time" name="time" id="time" value="<?php echo Helper_Index::time($data['time']); ?>" class="form-control">
            </div>
            <div class="form-group">
                <label for="destination" class="control-label"><?php echo __('dist') ?></label>
                <input type="text" name="destination" id="destination" value="<?php echo $data['destination']; ?>" class="form-control">
            </div>
            <div class="form-group">
                <label for="days" class="control-label"><?php echo __('days') ?></label>
                <select name="days" id="days" class="form-control">
                    <option value="all" <?php echo ($data['days'] == 'all' ? 'selected' : ''); ?>><?php echo __('days.all') ?></option>
                    <option value="work" <?php echo ($data['days'] == 'work' ? 'selected' : ''); ?>><?php echo __('days.work') ?></option>
                    <option value="weekend" <?php echo ($data['days'] == 'weekend' ? 'selected' : ''); ?>><?php echo __('days.weekend') ?></option>
                </select>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-success btn-block"><?php echo __('save') ?></button>
                <a href="/" class="btn btn-danger btn-block"><?php echo __('cancel') ?></a>
            </div>
        </form>
    </div>
</div>
