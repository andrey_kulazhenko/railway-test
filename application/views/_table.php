<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @var $data object Model_Scheldure
 * @var $allowControls bool Allow or Disallow control buttons
 */
?>

<table class="table table-striped table-hover">
    <thead>
    <tr>
        <th><?php echo __('time') ?></th>
        <th><?php echo __('dist') ?></th>
        <th><?php echo __('days') ?></th>
        <?php echo ($allowControls ? '<th></th>' : ''); ?>
    </tr>
    </thead>
    <tbody>
    <?php if($data->count()){
        foreach ($data as $d) { ?>
            <tr>
                <td><?php echo Helper_Index::time($d->time); ?></td>
                <td><?php echo $d->destination; ?></td>
                <td><?php echo Helper_Index::days($d->days); ?></td>
                <?php if($allowControls) { ?>
                    <td class="actions">
                        <a href="/edit/<?php echo $d->id; ?>" title="Редактировать расписание" data-toggle="tooltip"><i class="fa fa-lg fa-pencil"></i></a>
                        <a href="#" onclick="delItem(<?php echo $d->id; ?>);return false;" title="Удалить расписание" data-toggle="tooltip"><i class="fa fa-lg fa-remove"></i></a>
                    </td>
                <?php } ?>
            </tr>
        <?php }
    } else { ?>
        <tr>
            <td colspan="999">Ничего не найдено</td>
        </tr>
    <?php } ?>
    </tbody>
</table>
