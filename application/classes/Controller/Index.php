<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Index extends Controller_Basic {

    public function action_index(){
        $allowControls = true;
        $data = Model_Scheldure::getData();
        $table = View::factory('_table')
            ->bind('data',$data)
            ->bind('allowControls',$allowControls);
        $content = View::factory('index')->bind('table',$table);
        $this->template->header = 'Расписание электропоездов';
        $this->template->content = $content;
    }

    public function action_edit(){
        $this->template->header = 'Редактирование расписания';
        $item = Model_Scheldure::getItem($this->request->param('id'))->as_array();
        if($post = $this->request->post()){
            $validation = new Validation($post);
            $validation->label('time',__('time'))->rule('time','date');
            $validation->label('destination',__('dist'))->rule(TRUE,'not_empty');
            $validation->label('days',__('time'));
            if($validation->check()){
                Model_Scheldure::editItem($this->request->param('id'),$post);
                Helper_Message::set(Helper_Message::SUCCESS,__('edit.success'));
                $this->redirect(URL::base());
            }
            else{
                Helper_Message::set(Helper_Message::ERROR,$validation->errors('validation'));
            }
            $item = $this->request->post();
        }
        $item = Arr::extract($item,array('time','destination','days'),'');
        $content = View::factory('edit')
            ->bind('data',$item);
        $this->template->content = $content;
    }

    public function action_create(){
        $this->template->header = 'Создать новое расписание';
        $item = array();
        if($post = $this->request->post()){
            $validation = new Validation($post);
            $validation->label('time',__('time'))->rule('time','date');
            $validation->label('destination',__('dist'))->rule(TRUE,'not_empty');
            $validation->label('days',__('time'));
            if($validation->check()){
                Model_Scheldure::createItem($post);
                Helper_Message::set(Helper_Message::SUCCESS,__('create.success'));
                $this->redirect(URL::base());
            }
            else{
                Helper_Message::set(Helper_Message::ERROR,$validation->errors('validation'));
                $item = $this->request->post();
            }
        }
        $item = Arr::extract($item,array('time','destination','days'),'');
        $content = View::factory('edit')
            ->bind('data',$item);
        $this->template->content = $content;
    }

    public function action_search(){
        $allowControls = false;
        $this->template->header = 'Поиск Ближайшего Электропоезда';
        $query = $this->request->post('search') ? $this->request->post('search') : '';
        $founded = Model_Scheldure::findNearest($query);
        $table = View::factory('_table')
            ->bind('data',$founded)
            ->bind('allowControls',$allowControls);
        $content = View::factory('search')
            ->bind('table',$table)
            ->bind('query',$query);
        $this->template->content = $content;
    }

}