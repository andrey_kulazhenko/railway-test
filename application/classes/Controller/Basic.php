<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Basic extends Controller_Template {

    public $template = 'template';

    public function before() {
        parent::before();
        View::set_global('title', 'RailWay');
        View::set_global('description', 'RailWay Scheldure');
        $currentAction = $this->request->action();
        $sidebar = View::factory('sidebar')
            ->bind('action',$currentAction);
        $this->template->header = '';
        $this->template->sidebar = $sidebar;
        $this->template->content = '';
    }

}