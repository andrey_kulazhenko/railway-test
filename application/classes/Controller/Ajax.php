<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax extends Controller {

    public function before(){
        if(!$this->request->is_ajax()){
            throw new HTTP_Exception_403('You haven\'t permissions to access to this page!');
        }
    }

    public function action_delete_scheldure(){
        $req = json_decode($this->request->body());
        Model_Scheldure::delOne($req->id);
        $allowControls = true;
        $data = Model_Scheldure::getData();
        print_r(View::factory('_table')
            ->bind('data',$data)
            ->bind('allowControls',$allowControls)
            ->render());
    }

    public function action_filter_dist(){
        $req = json_decode($this->request->body());
        $allowControls = true;
        $data = Model_Scheldure::getData($req->filter);
        print_r(View::factory('_table')
            ->bind('data',$data)
            ->bind('allowControls',$allowControls)
            ->render());
    }

}