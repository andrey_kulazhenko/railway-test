<?php defined('SYSPATH') or die('No direct script access.');

class Model_Scheldure extends Model {

    public static function getData($filter = false){
        $data = ORM::factory('Table');
        if($filter){$data->where('destination','LIKE','%'.$filter.'%');}
        return $data->order_by('time')->find_all();
    }

    public static function delOne($id){
        ORM::factory('Table',$id)->delete();
    }

    public static function getItem($id){
        $item = ORM::factory('Table',$id);
        if(!$item->loaded()){
            throw new HTTP_Exception_404('Рассписание не найдено');
        }
        return $item;
    }

    public static function editItem($id,$data){
        ORM::factory('Table',$id)
            ->set('time',$data['time'])
            ->set('destination',$data['destination'])
            ->set('days',$data['days'])
            ->save();
    }

    public static function createItem($data){
        ORM::factory('Table')
            ->set('time',$data['time'])
            ->set('destination',$data['destination'])
            ->set('days',$data['days'])
            ->save();
    }

    public static function findNearest($query){
        return ORM::factory('Table')
            ->where('destination','LIKE','%'.$query.'%')
            ->and_where('time','>=',date("H:i:00"))
            ->and_where('days','IN',self::currentDay())
            ->order_by('time')
            ->limit(1)
            ->find_all();
    }

    private static function currentDay(){
        return date('N') <= 5 ? array('all','work') : array('all','weekend');
    }

}