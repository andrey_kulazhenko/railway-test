<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Message {

    const ERROR = 'danger';
    const SUCCESS = 'success';
    const INFO = 'info';

    /**
     * Set Notification Message with Type
     * @param $type
     * @param $message array()
     */
    public static function set($type,$message){
        if(is_string($message)){$message = array($message);}
        $messagesArray[$type] = $message;
        $session = Session::instance();
        $session->set('Notifications',$messagesArray);
    }

    /**
     * Return all Notifications by type
     */
    public static function showAlerts(){
        $session = Session::instance();
        $messagesArray = $session->get_once('Notifications');
        $out = '';
        if($messagesArray){
            $out = '';
            foreach ($messagesArray as $key => $value) {
                $out .= '<div class="alert alert-'.$key.' alert-dismissible">';
                $out .= '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                $out .= '<ul>';
                foreach ($value as $message) {
                    $out .= '<li>'.$message.'</li>';
                }
                $out .= '</ul></div>';
            }
            $out .= '';
        }
        return $out;
    }
}