<?php defined('SYSPATH') or die('No direct script access.');

class Helper_Index {

    public static function days($day){
        switch ($day){
            case 'work':
                $out = __('days.work');
                break;
            case 'weekend':
                $out = __('days.weekend');
                break;
            default:
                $out = __('days.all');
                break;
        }
        return $out;
    }

    public static function time($time){
        return !empty($time) ? date('H:i',strtotime($time)) : '';
    }

}